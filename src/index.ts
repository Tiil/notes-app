import bodyParser from "body-parser";
import express from "express";
import mongodb from "mongodb";

import { dbConfig } from "../config/mongoConfig";

import { routes } from "./app/routes";

const app = express();
app.use(bodyParser.json());

const MongoClient = mongodb.MongoClient;

MongoClient.connect(dbConfig.url, { useUnifiedTopology: true })
    .then((client) => {
        const db = client.db("NotesDatabase");

        routes(app, db);

        app.listen(8000);
    })
    .catch((error) => {
        // tslint:disable-next-line:no-console
        console.log(error);
    });
