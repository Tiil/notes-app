export interface INote {
    description: string;
    title: string;
}
