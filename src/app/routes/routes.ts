import { Express } from "express";
import { Db } from "mongodb";

import {
    crateNote,
    getAllNotes,
    getNoteById,
    updateNote,
    deleteNote,
} from "../contorllers";

export const routes = (app: Express, db: Db) => {
    const collection = db.collection("notes");

    app.post("/", (req, res) => crateNote(req, res, collection));

    app.get('/notes', (req, res) => getAllNotes(req, res, collection));

    app.get('/notes/:noteId', (req, res) => getNoteById(req, res, collection));

    app.put('/notes/:noteId', (req, res) => updateNote(req, res, collection));

    app.delete('/notes/:noteId', (req, res) => deleteNote(req, res, collection));
};
