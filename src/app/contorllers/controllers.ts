import { Request, Response } from "express";
import { Collection, ObjectID} from "mongodb";

import { INote } from "../models";

export const crateNote = (req: Request, res: Response, collection: Collection<INote>) => {
    if (req.headers["content-type"] !== "application/json") {
        res
            .status(400)
            .send({
                message: "Only Content-Type with value application/json permitted",
            });
    }

    if (!req.body.title || !req.body.description) {
        res
            .status(400)
            .send({
                message: "\"title\" and \"description\" fields must be provided",
            });
    }

    const note = {
        description: req.body.description,
        title: req.body.title,
    };

    collection.insertOne(note)
        .then(result => {
            res.status(200).send(result);
        })
        .catch( error => {
            console.log(error);
            console.log('Error occured while creating note');
        });

};

export const getAllNotes = (_req: Request, res: Response, collection: Collection<INote>) => {
    collection.find().toArray()
        .then(result => {
            res.status(200).send(result);
        })
        .catch( error => {
            console.log(error);
            console.log('Error occured while getting all notes');
        });
};

export const getNoteById = (req: Request, res: Response, collection: Collection<INote>) => {
    const id = req.params.noteId;

    collection.findOne({'_id': new ObjectID(id)})
        .then(result => {
            res.status(200).send(result);
        })
        .catch( error => {
            console.log(error);
            console.log('Error occured while getting note by id');
        });
};

export const updateNote = (req: Request, res: Response, collection: Collection<INote>) => {
    if (req.headers["content-type"] !== "application/json") {
        res
            .status(400)
            .send({
                message: "Only Content-Type with value application/json permitted",
            });
    }

    if (!req.body.title || !req.body.description) {
        res
            .status(400)
            .send({
                message: "\"title\" and \"description\" fields must be provided",
            });
    }

    const note = {
        description: req.body.description,
        title: req.body.title,
    };

    const id = req.params.noteId;

    collection.findOneAndReplace({'_id': new ObjectID(id)}, note, { returnOriginal: false })
        .then(note => {
            res.status(200).send(note)
        })
        .catch(error => {
            console.log(error);
            console.log('Error occured while updating note');
        });
};

export const deleteNote = (req: Request, res: Response, collection: Collection<INote>) => {
    const id = req.params.noteId;

    collection.findOneAndDelete({'_id': new ObjectID(id)})
        .then(() => {
            res.status(200).send();
        })
        .catch(error => {
            console.log(error);
            console.log('Error occured while updating note');
        });
};
