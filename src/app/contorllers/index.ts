export {
    crateNote,
    getAllNotes,
    getNoteById,
    updateNote,
    deleteNote,
} from "./controllers";
